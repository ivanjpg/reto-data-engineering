# WSMixer

## Reto técnico - Data Engineering

### Puesta en marcha

#### Local

Para ejecutar el sistema localmente, _Python_ debe estar instalado y posicionarse en el directorio de la aplicación. Si `pip` está disponible, puede   ejecutarse:

```bash
pip install -r requirements.txt
```

Al ser instalados los paquetes requeridos, el sistema puede ejecutarse como sigue:

```bash
cd wsmixer
python main.py
```

Los archivos de salida se encontrarán en el directorio `run_data`.

---

#### Docker Compose

Para poner en marcha el sistema utilizando _Docker Compose_, debe ejecutarse desde el directorio de la aplicación:

```bash
docker-compose up -d
```

Para identificar el contenedor se ejecuta:

```bash
docker ps -a
```

Sabiendo ahora el identificador del contenedor, se pueden visualizar los registros usando:

```bash
docker logs <ID>
```

Recordemos que el identificador puede ser parcial, inclusive sólo la primer letra si es que no existe ambigüedad.

Para localizar los volúmenes contenedores de los archivos, podemos escribir:

```bash
docker volume ls
```

Y luego, sabiendo el nombre del volumen, hacemos:

```bash
docker volume inspect <NOMBRE>
```

De la información que arroje el comando anterior, observamos la propiedad `Mountpoint`, que es el directorio _local_ donde podemos encontrar los archivos del contenedor.

Para eliminar al contenedor hacemos:

```bash
docker docker-compose down
```

Si queremos eliminar al contenedor y todos los volúmenes (**incluye todos los archivos generados**), el comando adecuado es:

```bash
docker docker-compose down --volumes
```

---

### Elección de tecnologías

En principio, cualquier lenguaje de programación de propósito general es suceptible de ser utilizado para una aplicación de esta naturaleza. Sin embargo, el corto tiempo de implementación, sugiere un lenguaje de programación de tipo _script_ por su versatilidad y rapidez probada apara realizar prototipos rápidos. Evitando así, una larga planeación para una propuesta o primer acercamiento a una posible solución de la cual aún no se determina su viabilidad, ni su probable tiempo de vida.

Si bien existen muchos lenguajes interpretados en la actualidad, los ecosistemas que los sostienen no son del todo uniformes, habiendo unos más activos y fuertes que otros. El auge de la Ciencia de Datos y sus campos relacionados dieron a [**Python**](https://www.python.org/) un nuevo impulso como favorito de la comunidad para la implementación de sus proyectos, apoyado por una creciente oferta de bibliotecas específicas para sus áreas de interés, así como por su constante actualización y comunidad de soporte. Cabe destacar que no únicamente se cuentan con herramientas y bibliotecas suficientes para la Ciencia de Datos, sino también para propósitos distintos que pudieran incluso converger en proyectos como el que nos atañe.

El poder de cómputo actual, inclusive en equipos caseros, ha sobrepasado con creces a los de años anteriores, por lo que métodos modernos que asisten a los ciclos de desarrollo y despliegue de sistemas han prosperado. Particularmente _entornos virtuales_ o _contenedores de software_ como [**Docker**](https://www.docker.com/) han resaltado en este aspecto. Docker, en particular, evita cabalmente el pretexto de antaño de _en mi equipo sí funciona_, basado en una descripción de los programas, versiones y estructura necesaria para la ejecución de los sistemas requeridos con las caracterísitcas justas y precisas sobre las que fueron desarrollados para funcionar, evitando confusiones y ambigüedades.

Hablando de los entornos donde se ejecutan las aplicaciones, la automatización de tareas de administración de los mismos resulta crucial para el buen funcionamiento de los programas, aún siendo no críticos. Al ser los entornos preferidos los tipo _\*nix_ (Linux, Unix, MacOS), la automatización de la administración mediante secuencias de comandos **SHELL** toma gran relevancia. Si bien la elección del intérprete de comandos es discrecional, una elección común es [**BASH**](https://www.gnu.org/software/bash/) por su sencilles y relativa _universalidad_.

Resumiendo las tecnologías globales que se usaron:

- [**Python**](https://www.python.org/) para la estructura general del sistema.
- [**Docker**](https://www.docker.com/) para el despliegue del aplicativo.
- [**BASH**](https://www.gnu.org/software/bash/) para las tareas de automatización y administración.

---

### Solución

#### Obtención de datos remotos

Como primer paso se consideró la obtención de la información proveniente del _webservice_. Para ello se utilizó la biblioteca [`requests`](https://requests.readthedocs.io/en/latest/), que proporciona un _API_ simple y útil para la interacción con el protocolo _HTTP_. Si bien a través del navegador, el _API_ de la CONAGUA funcionaba sin mayores inconvenientes, a través de `requests` se obtenía un error _403_, es decir, se estaba prohibiendo el acceso al recurso. La solución más simple (cuando es posible), consiste en establecer el encabezado _User-Agent_ del protocolo _HTTP_ para hacerse pasar por un navegador común. En este caso, funcionó sin mayor problema.

Más adelante en el desarrollo, se detectó que el _webservice_ no respondía de manera confiable a todas las peticiones que se realizaban, obteniéndose códigos _HTTP_ en los rangos _400s_ y _500s_. Para solventar esta contrariedad, se implementó una rutina que intenta un número fijo de veces obtener los datos del _webserver_. Si tiene éxito, continúa con el proceso, de lo contrario, registra el error y el sistema aborta todo el proceso. Además es importante establecer un _timeout_ para la conexión, ya que en ocasiones el _webservice_ mantenía la conexión, pero no otorgaba respuesta alguna.

Los datos devueltos por el servidor remoto se encuentran comprimidos en un archivo binario de tipo _gzip_. La instalación estándar de **Python** incluye el módulo [`gzip`](https://docs.python.org/3/library/gzip.html), lo que permite la descompresión en una linea de código. El proceso de descompresión entrega una cadena que contiene la información codificada en [_JSON_](https://www.json.org/json-en.html).

#### Proceso de los datos remotos

En este punto se tiene una cadena donde se encuentran codificados los datos como _JSON_. Para el tratamiento de los datos se eligió la biblioteca [`pandas`](https://pandas.pydata.org/), que ya incorpora un procesador de _JSON_ y permite cargarlo fácilmente hacia una estructura propia llamada [`DataFrame`](https://pandas.pydata.org/docs/reference/frame.html), que está inspirado en los [`data.frame`](https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/data.frame) del lenguaje [**R**](https://www.r-project.org/). Ahora tenemos una estructura _rectangular_ que podemos procesar utilizando las bondades de `pandas`.

También se requiere una copia de los datos remotos para su futuro consumo. Para ello se toma el `DataFrame` devuelto por `pandas` y se exporta, en este caso a un archivo tipo `pickle`, que es más eficiente al momento de procesarlo y ocupa menos espacio que un `csv`.

Claro, primero se debe hacer un análisis exploratorio de los datos obtenidos dado que se trata de una estructura externa _desconocida_. El primer problema encontrado es que las columnas que se documentan en [https://smn.conagua.gob.mx/es/web-service-api](https://smn.conagua.gob.mx/es/web-service-api) no coinciden con las representadas en los datos. A partir de los requerimientos del equipo de _DS_ se infiere que las columnas relevantes son:

- `ides`: Identificador del estado.
- `idmun`: Identificador del municipio.
- `temp`: Temperatura.
- `prec`: Precipitación.
- `hloc`: Fecha y hora a las que refiere el pronóstico.

El campo `hloc` es el primero en ser tratado. Se realiza una conversión del tipo de datos [`str`](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str) a [`datetime`](https://docs.python.org/3/library/datetime.html) para poder realizar un filtrado correcto y eficiente de los registros para obtener únicamente aquellos de las últimas dos horas, lo cual también se lleva a cabo, para posteriormente descartar las columnas que se presumen como no relevantes.

Como el requerimiento es obtener el promedio de temperatura y precipitación por estado y municipio, se utilizan funciones de agrupamiento de `pandas`, con lo que se alcanza uno de los requerimientos.

#### Obtención de datos locales

Se proporciona una carpeta llamada `data_municipios` con archivos organizados por fecha (en directorios). Se debe obtener el más reciente, lo cual se logra listando los archivos en el directorio correspondiente y ordenándolos. En este caso únicamente necesitamos la ruta y nombre del archivo. Si no existe una fecha que coincida con la fecha de ejecución, se toma el archivo con la fecha más reciente.

#### Proceso de datos locales

Como ya se tiene la ruta y nombre del archivo de datos locales con los datos más recientes, puede cargarse fácilmente con `pandas` debido a que el formato del archivo es _CSV_ y así como en el caso de _JSON_, `pandas` puede abrirlo y procesarlo sin problemas, mientras su formato interno sea válido, por supuesto.

De este proceso se obtiene un `DataFrame` con tres columnas, a saber:

- `Cve_Ent`: Clave de la entidad. Análogo correspondiente a `ides` de los datos provenientes del _webservice_.
- `Cve_Mun`: Clave del municipio. Cuya correspondencia en los datos remotos es el campo `idmun`.
- `Value`: Un valor de interés que se desea preservar.

#### Mezcla de datos remotos y locales

Según los requerimientos, los datos locales y remotos deben ser _cruzados_ para obtener una nueva tabla. Si bien, al tener un par de `DataFrames` y `pandas` tiene la capacidad de realizar el proceso, también es posible hacerlo directamente en _SQL_ a través de [_DuckDB_](https://duckdb.org/), un motor de bases de datos sin servidor que se integra a la perfección con `pandas` y permite realizar consultas utilizando _SQL_. En varios escenarios, _DuckDB_ supera en velocidad a `pandas`.

Para este caso se realiza un _LEFT JOIN_ estándar para obtener los datos cruzados que se requieren.

#### Modelo de despliegue

El despliegue de la aplicación se realiza mediante [_Docker Compose_](https://docs.docker.com/compose/). Se define un archivo llamado `docker-compose.yaml` con las instrucciones precisas para construir el entorno de ejecución. Si bien _Docker Compose_ está pensado para aplicaciones en _Docker_ que estén compuestas por múltiples contenedores, para este propósito particular, únicamente se requirió uno.

Como imagen base se elegió [_bullseye_](https://www.debian.org/News/2022/20221217), la última versión estable de [_Debian_](https://www.debian.org/). Esto, en contraposición a la elección común de [_Alpine_](https://www.alpinelinux.org/) debido a su enfoque en la seguridad. En nuestro caso, encontramos que _Alpine_ es poco eficiente para el trabajo con _Python_, en particular con los paquetes instalados usando [`pip`](https://pip.pypa.io/en/stable/), como lo es `pandas`.

Se realizó la definición de cuatro volúmenes virtuales para preservar los datos después de que el contenedor se detiene. Uno para los _logs_, otro para los datos locales, uno adicional usado para los datos remotos, y el principal para la aplicación misma. Además se montó un volumen que conecta con el _host_ para la copia de los datos locales, además de la aplicación.

Después de la creación del contenedor, se lanza un script de _Bash_ llamado `setuprun.sh` que realiza las siguientes acciones:

- Establece variables de entorno.
  - Una muy importante es la que identifica que nos encontramos dentro del contenedor de _Docker_. Dentro del sistema cambia el directorio base para una correcta adapatación al tiempo del desarrollo o producción.
- Copia los datos del sistema _host_ hacia el contenedor.
- Inicializa un entorno virtual de _Python_ con [`venv`](https://docs.python.org/3/library/venv.html).
- Actualiza `pip`.
- Instala las dependencias de _Python_ necesarias para la aplicación haciendo uso de `pip` y el archivo `requeriments.txt`.
- Para comprobar la instalación, se ejecuta el programa de _Python_ `test.py` que carga todas las bibliotecas que se requieren e imprime un mensaje en la salida estándar para comprobar que el entorno esté listo para iniciar el aplicativo.
- Inicia la ejecución de nuestra aplicación ejecutando `main.py`.

Destaquemos que se utiliza un archivo `.env` para gestionar variables importantes para el sistema que regulan el comportamiento del sistema. Todas ellas tienen un valor por default dentro de la aplicación, especificadas en `config.py`.

---

### Perspectivas

Mecionemos algunas consideraciones a futuro considerando que se decide mantener y escalar el sistema.

La base ineludible es el uso de un gestor de versiones como [_git_](https://git-scm.com/). Ya sea gestionado por algún proveedor externo como [_Github_](https://github.com/), [_Gitlab_](https://github.com/) o [_BitBucket_](https://bitbucket.org/), o en alguna implementación interna.

Además, debe considerarse el mantenimiento de la documentación. Si bien el proyecto contiene comentarios que ayudan a los desarrolladores a su mantenimiento, pronto resultarán insuficientes si el sistema entra en una fase de desarrollo activo. Como alternativas están los comentarios a modo de [`docstrings`](https://peps.python.org/pep-0257/) propios de _Python_, aunque herramientas más potentes como [_Sphinx_](https://www.sphinx-doc.org/en/master/) han cobrado popularidad por su facilidad de uso y versatilidad.

Al escalar la aplicación y las personas involucradas en su desarrollo, también se vuelve patente la necesidad de automatizar ciertas tareas como las pruebas (que no fueron incluidas en este prototipo rápido). Esta claro que el tipo de biblioteca o framework dependerá del lenguaje, pero en este caso al usar _Python_ podría elegirse la estándar y bien conocida biblioteca [`unittest`](https://docs.python.org/3/library/unittest.html), o las populares [`Robot`](https://robotframework.org/) o [`PyTest`](https://docs.pytest.org/en/7.3.x/). Las pruebas manuales son importantes, pero en una pieza de _software_ más sofisticada y donde la consistencia y fiabilidad son importantes, el uso de tecnologías adecuadas para las pruebas cobra un mayor valor.

Con la asignación de mayores recursos tanto humanos como tecnológicos, se requiere una organización y comunicación detalladas que permitan la correcta interacción y el registro de actividades y metas. El uso de herramientas como [_Jira_](https://www.atlassian.com/es/software/jira), [_Trac_](https://trac.edgewall.org/) algún otro software de gestión de proyectos es ideal. Para facilitar la comunicación, software especializado como [_Rocket.chat_](https://www.rocket.chat/) o [_Slack_](https://slack.com) pueden ser benéficos. Inclusive el adoptar metodologías de desarrollo pueden fomentar e incrementar la productividad. Hoy en día las metodologías _ágiles_ como _Kanban_ y _Scrum_ son las más populares, pero existe una amplia gama de opciones disponibles.

#### Mejoras a futuras versiones

Debido al tiempo de desarrollo, se tuvieron que tomar decisiones al respecto de qué aspectos omitir para centrarse en las funcionalidades plenas requeridas. Claro está que las mejoras pueden ser continuas, solventando las deficiencias del sistema. Entre las mejoras posibles están:

- Implementación de un sistema _externo_ de monitoreo para el aplicativo, que notifique cuando ocurra algún error irrecuperable. La finalidad es permitir a los administradores actuar en consecuencia lo más rápidamente posible.
- Mejora en el sistema de _loggeo_. El actual necesita de una variable global que se pasa como parámetro a prácticamente todas las funciones del sistema para lograr su objetivo. Quizás un primer enfoque sería agrupar funciones dentro de clases que encapsulen, inicialicen y operen elementos comunes como la variable de _logging_ para una mayor eficiencia y claridad en el código y sus posibles modificaciones y extensiones.
- Elección justificada de un formato de archivo de alamacenamiento de datos. Si bien el formato _CSV_ es un estándar simple y procesable fácilmente, cuando se involucran grandes cantidades de datos, el espacio ocupado y la velocidad de procesamiento se vuelven ineficientes rápidamente. Existen alternativas que pueden ser estudiadas para mejorar la fiabilidad, como son [_Parquet_](https://parquet.apache.org/), [_ORC_](https://orc.apache.org/), [_Avro_](https://avro.apache.org/), [_Pickle_](http://formats.kaitai.io/python_pickle/) o [_Feather_](https://arrow.apache.org/docs/python/feather.html). Varios de ellos pueden adaptarse de manera casi inmediata y trasparente para su proceso y almacenaje, así como para su uso por el equipo de DS.
- Reemplazo o mejora en el sistema de gestión de configuración. Si bien [`python-dotenv`](https://github.com/theskumar/python-dotenv) proporciona una alternativa simple y rápida de implementar, su estructura es lineal y siempre devuelve cadenas de caracteres, es decir, no gestiona tipos de datos. Entonces, puede sustituirse por otro sistema más potente. Algunas alternativas están basadas en _JSON_ o [_YAML_](https://yaml.org/), este último se utiliza para _Docker Compose_.

---