#!/usr/bin/env python

import schedule
from time import sleep
from os import getcwd

# Own modules
## Config
import wsmmod.config as mconfig
## Logging
import wsmmod.logging as log
## WebService consumption
import wsmmod.acquire as acq
## File-related utilities
import wsmmod.futils as fut
## Processing data routines
import wsmmod.processing as ps

# Main sequence, encapsulated for logging convenience
def main():
  logger.info('Starting a new cicle.')

  # Check the correct structure of the app
  ## External data directory
  logger.info(f'Verifying for external data dir *{cfg.EXTERNAL_DATA_DIR}*')
  fut.dir_exists(cfg.EXTERNAL_DATA_DIR, logger)

  ## Directory to save tables generated by this application
  logger.info(f'Verifying for own data dir *{cfg.OWN_DATA_DIR}*')
  fut.dir_exists_or_create(cfg.OWN_DATA_DIR, logger)
  ## And the «current» directory inside
  logger.info(f'Verifying for own current data dir *{cfg.OWN_CURRENT_DATA_DIR}*')
  fut.dir_exists_or_create(cfg.OWN_CURRENT_DATA_DIR, logger)

  ## Directory to save the downloaded data from the WebService
  logger.info(f'Verifying for WS downloaded data dir *{cfg.WS_DOWNLOADED_DATA_DIR}*')
  fut.dir_exists_or_create(cfg.WS_DOWNLOADED_DATA_DIR, logger)
  ## Also the existence of the «current» directory inside
  logger.info(f'Verifying for WS downloaded current data dir *{cfg.WS_DOWNLOADED_CURRENT_DATA_DIR}*')
  fut.dir_exists_or_create(cfg.WS_DOWNLOADED_CURRENT_DATA_DIR, logger)
  

  # Get the data from the WS
  ## Check if we must try again (this WS is prone to fail randomly)
  max_tries = 1
  wait_between_tries = 1
  if cfg.WS_TRY_IF_FAIL:
    try:
      max_tries = int(cfg.WS_TRY_MAX)
      wait_between_tries = int(cfg.WS_TRY_WAIT_TIME)
    except:
      logger.error(f'Failed to get config parameters for tries, defaults are 1.')

  bdata = None
  for ws_try in range(max_tries):
    if ws_try > 0:
      logger.info(f'Waiting {wait_between_tries} seconds before trying again...')
      sleep(wait_between_tries)

    logger.info(f'Try {ws_try+1}/{max_tries} to get data from WS.')
    
    try:
      bdata = acq.consume_ws(cfg, logger)
    except Exception as ex:
      logger.error(f'\tTry {ws_try+1} failed:\n\t{ex}')
    else:
      break
  
  if bdata is None:
    logger.error(f'After {max_tries} tries cannot get data from WS, aborting!')
    exit(-1)

  logger.info('Decompressing & decoding data from the webservice...')
  data = None

  try:
    # Decompress the GZip file obtained
    ddata = fut.deflate_gzip_bytes(bdata, logger)
    # Decode the deflated data
    data = ddata.decode()
  except Exception as ex:
    logger.error(f'Error in deflate-decode process: {ex}')
    exit(-1)

  # Get a DataFrame from JSON
  df = ps.get_df_from_json(data, logger)
  # Save the data for the historical record
  logger.info('Saving WebService data to local storage.')
  dnow, ddate, dhour = fut.get_dt_date_and_hour()
  fut.save_df_to_dir_and_current_as_file(df, 'pickle', dnow, cfg.WS_DOWNLOADED_DATA_DIR, cfg.WS_DOWNLOADED_CURRENT_DATA_DIR, logger)

  # Process to obtain a clean DataFrame
  df = ps.process_json_data(df, logger)  

  # Get the files inside the external data directory
  logger.info('Obtaining directories inside the external data dir.')
  efiles = fut.get_dir_dirs(cfg.EXTERNAL_DATA_DIR, logger)

  logger.info('\tSorting directories.')
  efiles = fut.sort_files_list(efiles, logger)

  if efiles[0] == ddate:
    logger.info('External data matches with today\'s date, everything up to date!')
  else:
    srecent = str(efiles[0]).split("/")[-1]
    logger.warning(f'The external data is outdated. Using the most recent set [{srecent}] for the mix!')

  # Loading CSV file into a DataFrame
  logger.info('Loading external data.')
  ext_df = ps.get_df_from_csv(f'{efiles[0]}/{cfg.DEFAULT_TABLE_FILENAME}', logger)

  # Get the «mixed» DataFrame
  logger.info('Mixing DataFrames.')
  mix_df = ps.get_mixed_df(df, ext_df, logger)

  # Save the «mixed» data
  logger.info('Saving mixed data to local storage.')
  fut.save_df_to_dir_and_current_as_file(df, 'csv', dnow, cfg.OWN_DATA_DIR, cfg.OWN_CURRENT_DATA_DIR, logger)

  # All done!
  logger.info('The whole process was completed!')

if __name__ != '__main__':
  exit(0)

# The base dirs for the data are different if we are on docker
## We set the default to the «run_data» directory inside
## the parent of the working directory.
base_dir = f'{getcwd()}/../run_data/'
## Checking if we are in docker and set BASE_DIR accordingly
if fut.are_we_inside_docker():
  # In the YAML we set the directories into the root
  base_dir = '/run_data/'

# Get the configuration from the .env file
cfg = mconfig.load_config(base_dir)

# Configure the logger
logger = log.setup_logger(cfg)
# Print the BASE_DIR to the logs
logger.info(f'The base dir for the data is: *{cfg.BASE_DIR}*')

# Decorator workaround to avoid importing issues
main = logger.catch(main) # type: ignore
# Schedule the execution of the application to every hour
logger.info('Running the first cycle...')
main()
logger.info('First cycle completed. Sleeping until the next one...')

# Schedule the next cycles
schedule.every(2).minutes.do(main)
schedule.every().hour.do(main)

# Loop for run pending jobs
while True:
  schedule.run_pending()
  # Sleep is necessary to avoid high non-sense CPU loads
  sleep(1)