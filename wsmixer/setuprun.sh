#!/usr/bin/env bash

export ARE_WE_INSIDE_DOCKER=1

DATA_BASE_DIR='/run_data'
HOST_DATA_DIR="/opt/app-data"
WSM_BASE_DIR="wsmixer"
WSM_DIR="/${WSM_BASE_DIR}"
PRCSV_BASE_DIR="data_municipios"
PRCSV_DIR="${DATA_BASE_DIR}/${PRCSV_BASE_DIR}"

check4error() {
  code=$1
  s=$2
  quit=${3:-true}

  if [ "${code}" != "0" ]; then
    echo "${s}"

    if [ ${quit} ]; then
      exit 1
    fi
  fi
}

echo "Copying data from the host to the container..."
cp -a "${HOST_DATA_DIR}/${WSM_BASE_DIR}"/. "${WSM_DIR}"
check4error $? "The core data cannot be copied, aborting!"
cp -a "${HOST_DATA_DIR}/${PRCSV_BASE_DIR}"/. "${PRCSV_DIR}"
check4error $? "The core data cannot be copied, aborting!"

echo "HOST_DATA_DIR=*${HOST_DATA_DIR}*"
echo "DATA_BASE_DIR=*${DATA_BASE_DIR}*"
echo "WSM_DIR=*${WSM_DIR}*"
echo "PRCSV_DIR=*${PRCSV_DIR}*"

echo "Setting up VENV inside the application dir *${WSM_DIR}*..."
python -m venv "${WSM_DIR}"
check4error $? "VENV cannot be placed in the app dir!"

echo "Activating VENV..."
source "${WSM_DIR}/bin/activate"
check4error $? "Activation of VENV failed!"

echo "Upgrading PIP..."
pip install --upgrade pip
check4error $? "PIP cannot be upgraded" false

echo "Installing Python requeriments..."
pip install -r ${WSM_DIR}/requirements.txt
check4error $? "Python requirements cannot be installed!"

echo "Testing Python packages installation..."
chmod a+x ${WSM_DIR}/test.py
${WSM_DIR}/test.py
check4error $? "Test script was now executed!"

echo "Starting the system..."
pushd ${WSM_DIR}
chmod a+x main.py
./main.py
check4error $? "The system was terminated with error!"