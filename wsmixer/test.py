#!/usr/bin/env python

from loguru import logger
import requests
import gzip
import pandas as pd
import duckdb as ddb
from datetime import datetime, timedelta
import schedule

print(f'Hi from docker: {datetime.now()}')