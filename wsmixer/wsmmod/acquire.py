import requests

# For annotated types
from loguru._logger import Logger
from types import SimpleNamespace

# The names of the headers are all first chars of the words capitalized,
# but it is not required, as the standard for headers are that are not
# case insensitive.
WS_RESPONSE_HEADERS_TO_PRINT = [
  'Content-Type',
  'Last-Modified',
  'Content-Disposition',
  'Content-Transfer-Encoding',
  'Content-Length'
]

def consume_ws(config :SimpleNamespace, logger :Logger) -> bytes:
  logger.info('Requesting data from the WebService')
  
  # Need to use a desktop user agent to avoid a 403 code from the WS
  headers = {
    'User-Agent': config.WS_USER_AGENT,
  }

  logger.info(f'Requesting endpoint: {config.WS_ENDPOINT}')
  res = requests.get(config.WS_ENDPOINT, headers=headers, timeout=int(config.WS_REQ_TIMEOUT))
  
  if res.status_code != requests.codes.ok:
    raise Exception(f'The response from the WebService was not succesful, code: {res.status_code}')

  logger.info('Succesful response from the WebService.')
  for hname in WS_RESPONSE_HEADERS_TO_PRINT:
    val = res.headers[hname]
    logger.info(f'\t{hname}: {val}')

  return res.content