from dotenv import dotenv_values
from types import SimpleNamespace
from importlib import import_module

all_vars = {
  # The ABS_ prefix means that it needs to prepend
  # the base directory.
  'APP_NAME': 'WSMixer',

	'WS_ENDPOINT': 'https://smn.conagua.gob.mx/webservices/index.php?method=1',
	'WS_USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_3_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36',
	'WS_TRY_IF_FAIL': True,
	'WS_TRY_MAX': 4,
	'WS_TRY_WAIT_TIME': 4,
  'WS_REQ_TIMEOUT': 10,

	'ABS_LOGS_DIR': 'wsmixer-logs/',
	'LOGS_TO_CONSOLE': True,
	'LOGS_FILES_MAX_SIZE': '10 MB',
	'LOGS_LEVEL': 'DEBUG',

  'ABS_WS_DOWNLOADED_DATA_DIR': 'ws_downloaded_data/',
  'ABS_WS_DOWNLOADED_CURRENT_DATA_DIR': 'current',
	'ABS_EXTERNAL_DATA_DIR': 'data_municipios/',
	'ABS_OWN_DATA_DIR': 'data_generated/',
	'ABS_OWN_CURRENT_DATA_DIR': 'current',

	'DEFAULT_TABLE_FILENAME': 'data.csv',
}

all_output_types = {
  'csv': {
    'ext': 'csv',
    'fn': 'to_csv',
  },
  'pickle': {
    'ext': 'pickle',
    'fn': 'to_pickle',
  },
  'json': {
    'ext': 'json',
    'fn': 'to_json',
  },
  'xml': {
    'ext': 'xml',
    'fn': 'to_xml',
    'deps': [
      'pyarrow',
    ],
  },
  'orc': {
    'ext': 'orc',
    'fn': 'to_orc',
    'deps': [
      'pyarrow',
    ],
  },
  'feather': {
    'ext': 'feather',
    'fn': 'to_feather',
    'deps': [
      'pyarrow',
    ],
  },
  'parquet': {
    'ext': 'parquet',
    'fn': 'to_parquet',
    'deps': [
      'pyarrow',
    ],
  },
}

def get_ouput_type(type :str, fallback :bool=True) -> dict:
  if type not in all_output_types.keys():
    if fallback:
      return all_output_types['csv']
    else:
      return {}

  return all_output_types[type]

def setup_output_type(type :str) -> bool:
  if type not in all_output_types.keys():
    return False
  
  fmt = all_output_types[type]
  if not fmt.has_key('deps'):
    return True

  for dep in fmt['deps']:
    try:
      import_module(dep)
    except:
      return False

  return True

def load_config(base_dir :str = '') -> SimpleNamespace:
  # Get the configuration vars
  config = all_vars | dotenv_values('./.env')

  cfg = {}

  for var in all_vars:
    varname = var
    prepend = ''

    if 'ABS_' in var:
      varname = var.split('ABS_')[1]
      prepend = base_dir      

    cfg[varname] = f'{prepend}{config[var]}'

    print(f'Setting var *{varname}* to *{cfg[varname]}*')

  cfg['BASE_DIR'] = base_dir

  return SimpleNamespace(**cfg)