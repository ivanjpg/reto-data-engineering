import gzip

from os import mkdir, environ, remove
from os.path import isdir
from os.path import join as pjoin

from datetime import datetime

from pathlib import Path

from inspect import signature

from . import config as mcfg

# For annotated types
from loguru._logger import Logger
import pandas as pd
from types import SimpleNamespace

def deflate_gzip_bytes(data :bytes, logger :Logger) -> bytes:
  logger.info(f'About to decompress {len(data)} bytes of data.')

  return gzip.decompress(data)

def dir_exists(dir :str, logger :Logger) -> bool:
  res = isdir(dir)

  logger.info(f'Directory *{dir}* exists.')

  return res

def create_dir(dir :str, logger :Logger) -> None:
  logger.info(f'Creating directory *{dir}*.')

  mkdir(dir)

def dir_exists_or_create(dir :str, logger :Logger) -> None:
  if dir_exists(dir, logger): return

  create_dir(dir, logger)

def get_dir_list(dir :str, logger :Logger, files :bool = True, dirs :bool = True) -> list:
  logger.info(f'Getting list contents from *{dir}*')

  content = []

  for e in Path(dir).iterdir():
    if files and e.is_file(): content.append(e)
    if dirs and e.is_dir(): content.append(e)

  logger.info(f'\tContent found: {content}')

  return content

def get_dir_dirs(dir :str, logger :Logger) -> list:
  logger.info('Getting dirs.')

  return get_dir_list(dir, logger, files=False, dirs=True)

def get_dir_files(dir :str, logger :Logger) -> list:
  logger.info('Getting files.')

  return get_dir_list(dir, logger, files=True, dirs=False)

def sort_files_list(files :list, logger :Logger) -> list:
  logger.info('Sorting a list of files.')

  files.sort(reverse=True)

  logger.info(f'\tSorted files list: {files}')

  return files

def save_bytes_to_dir(data :bytes, fname :str, dir :str, logger :Logger) -> bool:
  fpath = pjoin(dir, fname)

  res = False
  try:
    fd = open(fpath, 'wb')
    fd.write(data)
    fd.close()

    res = True
  except Exception as ex:
    logger.error(f'Error writing file *{fpath}*: {ex}')

  return res

def save_df_to_file(df :pd.DataFrame, type :str, fname: str, dir :str, logger :Logger) -> bool:
  fmt = mcfg.get_ouput_type(type)

  fpath = pjoin(dir, f'{fname}.{fmt["ext"]}')

  res = False
  dfn = df.reset_index(drop=True)

  try:
    fn = getattr(dfn, fmt['fn'])

    if 'index' in signature(fn).parameters.keys():
      fn(fpath, index=False)
    else:
      fn(fpath)

    res = True
  except Exception as ex:
    logger.error(f'Error writing pickle dataframe *{fpath}*: {ex}')

  return res

def save_df_to_pickle(df :pd.DataFrame, fname: str, dir :str, logger :Logger) -> bool:
  return save_df_to_file(df, 'pickle', fname, dir, logger)

def save_df_to_csv(df :pd.DataFrame, fname: str, dir :str, logger :Logger) -> bool:
  return save_df_to_file(df, 'csv', fname, dir, logger)

def remove_files_from_dir(dir :str, logger :Logger) -> bool:
  res = False

  logger.info(f'Removing files of the dir: *{dir}*')
  for f in get_dir_files(dir, logger):
    fname = pjoin(dir, f)

    try:
      remove(fname)
      res = True
    except Exception as ex:
      res = False
      logger.error(f'Failed to remove the file: *{fname}*: {ex}')

  
  return res

def save_df_to_dir_and_current_as_file(df :pd.DataFrame, type :str, dnow :str, dir :str, current :str, logger :Logger) -> bool:
  # Clean the «current» directory
  remove_files_from_dir(current, logger)

  fname = f'{dnow}-data'

  # Saving to «current»
  res = save_df_to_file(df, type, fname, current, logger)

  # Check for directory with the date
  d = dnow.split('T')[0]
  new_dir = pjoin(dir, d)
  dir_exists_or_create(new_dir, logger)
  
  # Saving to historical
  return save_df_to_file(df, type, fname, new_dir, logger) and res

def are_we_inside_docker() -> bool:
  val = environ.get('ARE_WE_INSIDE_DOCKER')
  
  if val == '1' or val == 'True':
    return True
  
  return False

def get_dtnow_as_str() -> str:
  return datetime.now().strftime('%Y%m%dT%H%M%S')

def get_dt_date_and_hour() -> list:
  now = get_dtnow_as_str()
  
  return [now, *now.split('T')]