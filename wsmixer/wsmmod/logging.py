from loguru import logger

from types import SimpleNamespace

# For annotated types
from loguru._logger import Logger

def setup_logger(config :SimpleNamespace) -> Logger: 
  if not config.LOGS_TO_CONSOLE:
    logger.remove()

  # Add new handler to log messages to a file
  logger.add(f'{config.LOGS_DIR}/{config.APP_NAME}.log', colorize=True, format='<green>{time}</green> <level>{message}</level>', level=config.LOGS_LEVEL, rotation=config.LOGS_FILES_MAX_SIZE)

  logger.info('The log system has been initialized.')

  return logger # type: ignore