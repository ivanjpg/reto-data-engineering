from datetime import datetime, timedelta
import pandas as pd
import duckdb as ddb

# For annotated types
from loguru._logger import Logger

def get_df_from_json(json :str, logger :Logger) -> pd.DataFrame:
  logger.info('Processing JSON.')
  
  return pd.read_json(json)

def process_json_data(df :pd.DataFrame, logger :Logger) -> pd.DataFrame:
  logger.info('Processing DataFrame.')

  total_records = len(df.index)
  logger.info(f'Obtained {total_records} records with {df["ides"].unique().size} states, and {df["idmun"].unique().size} municipalities.')

  logger.info('Removing unwanted columns.')
  old_size = df.memory_usage().sum()
  
  cols2drop = set(df.columns) - set([
    'ides',
    'idmun',
    'temp',
    'prec',
    'hloc',
  ])

  df.drop(list(cols2drop), axis=1, inplace=True)
  new_size = df.memory_usage().sum()

  logger.info(f'After dropping columns the size of the DataFrame goes from {old_size} to {new_size}. This is: {new_size/old_size*100:.1f}% less')

  logger.info('Converting DateTime column to appropiate data type.')
  df['hloc'] = df['hloc'].apply(lambda x: datetime.strptime(x, '%Y%m%dT%H'))

  logger.info('Selecting only records from 2 hours to now.')

  # New DataFrame for remaining records
  ndf = pd.DataFrame()
  # Grouping records
  rgroups = df.groupby(by=['ides','idmun'])
  # Process each group
  for gname in rgroups.groups.keys():
    # Get the last two records (2 last hours)
    last_two = rgroups.get_group(gname).sort_values(by=['hloc'], ascending=False).iloc[0:2]
    # To DataFrame and remove «hloc» column
    ldf = last_two.mean().to_frame().T.drop('hloc', axis=1)
    # Add to the new DataFrame
    if ndf.empty:
      ndf = ldf.copy()
    else:
      ndf = pd.concat([ndf,ldf])

  # Convert the ID's datatype
  for col in ['ides', 'idmun']:
    ndf[col] = ndf[col].astype(int)

  remaining_percentaje = len(ndf.index) / total_records * 100
  logger.info(f'Keeping {len(ndf.index)}/{total_records} records [{remaining_percentaje:.1f}%] after filtering.')

  logger.info('DataFrame cleaned & processed!')

  # Return the processed json data as a DataFrame
  return ndf

def get_df_from_csv(fname :str, logger :Logger) -> pd.DataFrame:
  logger.info(f'Loading data from *{fname}* into a DataFrame.')

  return pd.read_csv(fname)

def get_mixed_df(wsdf :pd.DataFrame, exdf :pd.DataFrame, logger :Logger) -> pd.DataFrame:
  logger.info('Mixing two DataFrames using DuckDB')

  return ddb.query('''
    SELECT
      Cve_Ent,
      Cve_Mun,
      Value,
      temp as Temp,
      prec as Prec
    FROM
      wsdf
    LEFT JOIN
      exdf
    ON
      Cve_Ent = ides
        AND
      Cve_Mun = idmun
  ''').to_df()
